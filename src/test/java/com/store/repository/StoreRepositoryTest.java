package com.store.repository;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mona.CauseCodeTestApplication;
import com.mona.domain.Store;
import com.mona.repository.StoreRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CauseCodeTestApplication.class)
public class StoreRepositoryTest {
	
	@Autowired
	private StoreRepository repository;

	@Test
	public void testFindByName() {
		List<Store> stores = repository.findByName("Mac1");
		assertTrue(stores.size() == 1);
	}

}