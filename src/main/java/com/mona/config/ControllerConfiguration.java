package com.mona.config;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
/*
 * To handle the Domain level constraints violation Exceptions.
 */
@ControllerAdvice
public class ControllerConfiguration {
	
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public void handleConstraintViolation(){
            
		System.out.println("Contraint Violated !!");
    }

}