package com.mona.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.mona.domain.Store;
import com.mona.service.StoreService;
import com.mona.util.StoreNotFoundException;

@RestController
@RequestMapping("/store")
public class StoreController {

	@Autowired
	private StoreService storeService;

	/*
	 * Method to create a store entity in the database.
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	public Store create(@Valid @RequestBody Store store) {
		return storeService.save(store);
	}

	/*
	 * Method to find all the stores in the database.
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public List<Store> getAll() {
		return storeService.findAll();
	}

	/*
	 * Method to get a store from database of specified id.
	 */
	@RequestMapping(value = "/get/{storeId}", method = RequestMethod.GET)
	public Store get(@PathVariable Long storeId) {
		return storeService.findOne(storeId);
	}

	/*
	 * Method to find a store with the given name.
	 */
	@RequestMapping(value = "/findByName/{storeName}", method = RequestMethod.GET)
	public Store getByName(@PathVariable String storeName) {
		Store store = storeService.findByName(storeName);
		return store;
	}

	/*
	 * Method to delete a store entity.
	 */
	@RequestMapping(value = "/delete/{storeId}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long storeId) {

		storeService.delete(storeId);
	}

	/*
	 * Method to update a store entity.
	 */
	@RequestMapping(value = "/update/{storeId}", method = RequestMethod.PATCH)
	public Store update(@PathVariable Long storeId, @Valid @RequestBody Store store) {

		Store storeFromDb = storeService.findOne(storeId);

		if (storeFromDb != null)
			merge(store, storeFromDb);
		else
			throw new StoreNotFoundException("could not find store with id :" + storeId.toString());
		return storeService.save(storeFromDb);

	}

	/*
	 * Method to search near by stores 'x' miles from a given location with
	 * provided zipcode.
	 *  Test in Postman -
	 * http://localhost:8080/store/search?zipcode=10036&miles=10
	 */

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public Set<Store> findNearByStores(@RequestParam String zipcode, @RequestParam int miles) {
		return storeService.findWithinMilesOfZipcode(zipcode, miles);
	}

	private void merge(Store store, Store storeFromDb) {
		if (store.getName() != null)
			storeFromDb.setName(store.getName());
		if (store.getBranchName() != null)
			storeFromDb.setBranchName(store.getBranchName());
		if (store.getZipCode() != null)
			storeFromDb.setZipCode(store.getZipCode());
		if (store.getLatitude() != null)
			storeFromDb.setLatitude(store.getLatitude());
		if (store.getLongitude() != null)
			storeFromDb.setLongitude(store.getLongitude());
	}

}
