package com.mona.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mona.domain.Store;

public interface StoreRepository extends JpaRepository<Store, Long> {

	public List<Store> findByName(String name);
}
