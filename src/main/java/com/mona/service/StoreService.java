package com.mona.service;

import java.util.List;
import java.util.Set;

import com.mona.domain.Store;

public interface StoreService {

	public Store findByName(String name);

	public Store save(Store store);

	public List<Store> findAll();

	public Store findOne(Long storeId);

	public void delete(Long storeId);

	public Set<Store> findWithinMilesOfZipcode(String zipcode, int miles);

}
