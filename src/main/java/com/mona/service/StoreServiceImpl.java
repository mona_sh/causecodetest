package com.mona.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mona.domain.Store;
import com.mona.domain.google.GoogleMapResults;
import com.mona.domain.google.Location;
import com.mona.repository.StoreRepository;
import com.mona.util.DistanceCalculator;
import com.mona.util.StoreNotFoundException;

@Service
public class StoreServiceImpl implements StoreService {

	public static final String GOOGLE_BASE_URL = "https://maps.googleapis.com/maps/api/geocode/json?address=";
	// sample url of time square =
	// https://maps.googleapis.com/maps/api/geocode/json?address=10036

	@Autowired
	private StoreRepository repository;
	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public Store findByName(String name) {
		List<Store> stores = repository.findByName(name);
		Store store = null;
		if (stores.isEmpty()) {
			throw new StoreNotFoundException(" could not find store " + name);
		} else {
			store = stores.get(0);
		}
		return store;
	}

	@Override
	public Store save(Store input) {
		return repository.save(input);
	}

	@Override
	public List<Store> findAll() {
		return repository.findAll();
	}

	@Override
	public Store findOne(Long storeId) {

		Store storeFromDb = repository.findOne(storeId);
		if (storeFromDb == null)
			throw new StoreNotFoundException(" could not find store with id :" + storeId.toString());
		else
			return storeFromDb;
	}

	@Override
	public void delete(Long storeId) {

		Store storeFromDb = repository.findOne(storeId);
		if (storeFromDb != null)
			repository.delete(storeId);
		else
			throw new StoreNotFoundException("could not find store with id :" + storeId.toString());
	}

	@Override
	public Set<Store> findWithinMilesOfZipcode(String zipcode, int miles) {
		GoogleMapResults results = restTemplate.getForObject(GOOGLE_BASE_URL + zipcode, GoogleMapResults.class);
		Set<Store> stores = new HashSet<>();
		Location zipLocation = null;
		if (results.getResults() != null && !results.getResults().isEmpty()) {
			zipLocation = results.getResults().get(0).getGeometry().getLocation();
			System.out.println("ziplocation : " + zipLocation);
			List<Store> allStores = repository.findAll();
			System.out.println("all stores : " + allStores);
			for (Store store : allStores) {
				double distance = DistanceCalculator.distance(zipLocation.getLat(), zipLocation.getLng(),
						store.getLatitude(), store.getLongitude(), "M");
				if (distance < miles) {
					stores.add(store);
				}
			}
		}
		System.out.println(stores);
		return stores;
	}

}