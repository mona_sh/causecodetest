package com.mona.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
/*
 *  Domain class for store
 */
@Entity
public class Store {

	@Id
	@GeneratedValue
	private Long id;
	@NotBlank
	private String name;
	@NotNull
	private String branchName;
	private String zipCode;
	@NotNull
	private Double longitude;
	@NotNull
	private Double latitude;

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Store [id=" + id + ", name=" + name + ", branchName=" + branchName + ", lattitude=" + latitude
				+ ", longitude=" + longitude + ", zipCode=" + zipCode + "]";
	}

}
