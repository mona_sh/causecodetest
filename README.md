#README#

###1>Prerequisites ###

Java 8

MySQL DB

Maven 3.0+

### 2>Change application.properties with MySQL jdbc url, username and password ###

### 3> Steps ###
1. In root folder run mvn install (It will create a jar file in target folder)
2. run following command in shell
java -jar target/store-0.0.1-SNAPSHOT.jar
3. Above will start embedded tomcat

**Test using POSTMAN - REST Client shortcut**
### CRUD operation: ###

1. Create Store - Method = POST Data {} URL = http://localhost:8080/store/create   , RequestBody : Store as JSON form
2. Find Store by Id - Method = GET Data {} URL = http://localhost:8080/store/get/1
3. Find All Stores - Method = GET Data {} URL = http://localhost:8080/store/getAll
4. Find by name - Method = GET Data {} URL = http://localhost:8080/store/findByName/Mac1
5. Update a store - Method = PATCH Data {} URL = http://localhost:8080/store/update/2
6. Delete a store by id - Method = DELETE Data {} URL = http://localhost:8080/store/delete/2

### Find stores within 'x' miles of USA zipcode. ###

Search store - Method = GET Data {} URL = http://localhost:8080/store/search?zipcode=10036&miles=10

Test Cases Class Name

Controller                                                             
Repository       
Service